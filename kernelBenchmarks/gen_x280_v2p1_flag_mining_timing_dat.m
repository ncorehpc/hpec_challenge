cd cfar/data;
writeFile('1-cfar-timing.dat', 0.000602, 'float32');

cd ../../ct/data;
writeFile('1-ct-timing.dat', 0.002667, 'float32');

cd ../../db/data;
writeFile('1-db-timing.dat', 0.004719, 'float32');
 
cd ../../fdfir/data;
writeFile('1-fdFir-time.dat', 0.05511, 'float32');

cd ../../pm/data;
writeFile('1-pm-timing.dat', 0.002244, 'float32');
writeFile('2-pm-timing.dat', 0.015032, 'float32');

cd ../../svd/data;
writeFile('1-svd-timing.dat', [0.400775 130], 'float32');
writeFile('2-svd-timing.dat', [0.057244 81], 'float32');
writeFile('3-svd-timing.dat', [0.581189 170], 'float32');

cd ../../tdfir/data;
writeFile('1-tdFir-time.dat', 0.731835, 'float32')