cd cfar/data;
writeFile('1-cfar-timing.dat', 0.0006, 'float32');

cd ../../ct/data;
writeFile('1-ct-timing.dat', 0.002854, 'float32');

cd ../../db/data;
writeFile('1-db-timing.dat', 0.0047, 'float32');
 
cd ../../fdfir/data;
writeFile('1-fdFir-time.dat', 0.056065, 'float32');

cd ../../pm/data;
writeFile('1-pm-timing.dat', 0.00267, 'float32');
writeFile('2-pm-timing.dat', 0.024551, 'float32');

cd ../../svd/data;
writeFile('1-svd-timing.dat', [0.404917 130], 'float32');
writeFile('2-svd-timing.dat', [0.056459 81], 'float32');
writeFile('3-svd-timing.dat', [0.60155 170], 'float32');

cd ../../tdfir/data;
writeFile('1-tdFir-time.dat', 0.802728, 'float32')



 

