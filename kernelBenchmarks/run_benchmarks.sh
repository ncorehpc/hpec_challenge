#!/bin/sh

echo "================ hpec_challenge =============="
echo Date: `date`
echo Kernel: `uname -a`
echo ""
#lscpu
echo ""
echo "-- Compiler options:"
echo ""

echo ""
echo ""

##DIRS=(      "cfar" "ct" "db" "fdfir" "ga"     "pm" "qr" "svd" "tdfir")  # Directories
##exec_names=("cfar" "ct" "db" "fdFir" "genalg" "pm" "qr" "svd" "tdFir")  # Executable name
##ndata=(      4      2    2    2       4        2    3    3     2)       # Number of datasets for each kernel
##
### number of kernels
##let NUM_DIRS=${#DIRS[@]}-1
##
##for i in $(seq 0 $NUM_DIRS);  # for each kernel
##do
##	cd ${DIRS[$i]} #change to that directory
##	pwd
##	count=${ndata[$i]}
##	for dataset in $(seq 1 $count);
##	do
##		echo ${exec_names[$i]} " " $dataset  # echo kernel and dataset
##		"${exec_names[$i]}" $dataset # run kernel / dataset
##		"${exec_names[$i]}""Verify" $dataset #run verifiy dataset
##	done
##	cd ../ # return to top directory
##done

# The above nice script doesn't run on platform due to an older /bin/sh (I guess)
# Hardcoding the script here for the moment ...

cd cfar
echo "cfar 1-4"
./cfar 1
./cfarVerify 1
./cfar 2
./cfarVerify 2
./cfar 3
./cfarVerify 3
./cfar 4
./cfarVerify 4
cd ..

cd ct
echo "ct 1-2"
./ct 1
./ctVerify 1
./ct 2
./ctVerify 2
cd ..

cd db
echo "db 1-2"
./db 1
./dbVerify 1
./db 2
./dbVerify 2
cd ..

cd fdfir
echo "fdfir 1-2"
./fdFir 1
./fdFirVerify 1
./fdFir 2
./fdFirVerify 2
cd ..

cd ga
echo "genalg 1-4"
./genalg 1
./genalgVerify 1
./genalg 2
./genalgVerify 2
./genalg 3
./genalgVerify 3
./genalg 4
./genalgVerify 4
cd ..

cd pm
echo "pm 1-2"
./pm 1
./pmVerify 1
./pm 2
./pmVerify 2
cd ..

cd qr
echo "qr 1-3"
./qr 1
./qrVerify 1
./qr 2
./qrVerify 2
./qr 3
./qrVerify 3
cd ..

cd svd
echo "svd 1-3"
./svd 1
./svdVerify 1
./svd 2
./svdVerify 2
./svd 3
./svdVerify 3
cd ..

cd tdfir
echo "tdfir 1-2"
./tdFir 1
./tdFirVerify 1
./tdFir 2
./tdFirVerify 2
cd ..
